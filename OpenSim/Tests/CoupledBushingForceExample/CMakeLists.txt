
# Variable definitions
SET(EXAMPLE_TARGET exampleCoupledBushingForcePlugin)
SET(EXAMPLE_DIR ${OpenSim_SOURCE_DIR}/OpenSim/Examples/Plugins/CoupledBushingForceExample)

INCLUDE_DIRECTORIES(${OpenSim_SOURCE_DIR} 
		    ${OpenSim_SOURCE_DIR}/Vendors)
			
FILE(GLOB SOURCE_FILES ${EXAMPLE_DIR}/*.h ${EXAMPLE_DIR}/*.cpp)
ADD_LIBRARY(${EXAMPLE_TARGET} SHARED ${SOURCE_FILES})

TARGET_LINK_LIBRARIES(${EXAMPLE_TARGET}
        osimCommon
		osimSimulation
		osimActuators
		osimTools
		osimAnalyses
	${SIMTK_ALL_LIBS}
)
SET_TARGET_PROPERTIES(${EXAMPLE_TARGET}
	PROPERTIES
	DEFINE_SYMBOL OSIMPLUGIN_EXPORTS
	PROJECT_LABEL "Examples - ${EXAMPLE_TARGET}"
	RUNTIME_OUTPUT_DIRECTORY ${OpenSim_BINARY_DIR}/OpenSim/Examples/Plugins/CoupledBushingForceExample
)
